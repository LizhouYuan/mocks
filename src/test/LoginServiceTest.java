package test;

import main.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import org.junit.Before;
import org.junit.Test;

public class LoginServiceTest {
 
	private IAccount account;
	private IAccountRepository accountRepository;
	private LoginService service;

	/*
	Initialize the mocks/stubs using mock method
	 */
	@Before
	public void init() {
	   account = mock(IAccount.class);
	   accountRepository = mock(IAccountRepository.class);
	   when(accountRepository.find(anyString())).thenReturn(account);
	   service = new LoginService(accountRepository);
	}

    private void willPassword24Previous(boolean value) {
        when(account.password24Previous(anyString())).thenReturn(value);
    }
	private void willPasswordMatch(boolean value) {
	   when(account.passwordMatches(anyString())).thenReturn(value);
	}

    private void willPasswordExpire(boolean value) {
        when(account.passwordExpired(anyString())).thenReturn(value);
    }

    private void willPasswordTemporary(boolean value) {
        when(account.passwordTemporary(anyString())).thenReturn(value);
    }

	@Test
	public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
	   willPasswordMatch(true);
	   service.login("brett", "password");
	   verify(account, times(1)).setLoggedIn(true);
	}

	/*
	Failed logins causes account to be revoked
	 */
	@Test
	public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
	   willPasswordMatch(false);

	   for (int i = 0; i < 3; ++i)
	      service.login("brett", "password");

	   verify(account, times(1)).setRevoked(true);
	}

	@Test
	public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
	   willPasswordMatch(false);
	   service.login("brett", "password");
	   verify(account, never()).setLoggedIn(true);
	}

	@Test
	public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
	   willPasswordMatch(false);

	   IAccount secondAccount = mock(IAccount.class);
	   when(secondAccount.passwordMatches(anyString())).thenReturn(false);
	   when(accountRepository.find("schuchert")).thenReturn(secondAccount);

	   service.login("brett", "password");
	   service.login("brett", "password");
	   service.login("schuchert", "password");

	   verify(secondAccount, never()).setRevoked(true);
	}

	@Test(expected = AccountLoginLimitReachedException.class)
	public void itShouldNotAllowConcurrentLogins() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(true);
	   service.login("brett", "password");
	}

	@Test(expected = AccountNotFoundException.class)
	public void itShouldThrowExceptionIfAccountNotFound() {
	   when(accountRepository.find("schuchert")).thenReturn(null);
	   service.login("schuchert", "password");
	}

	@Test(expected = AccountRevokedException.class)
	public void itShouldNotBePossibleToLogIntoRevokedAccount() {
	   willPasswordMatch(true);
	   when(account.isRevoked()).thenReturn(true);
	   service.login("brett", "password");
	}

	/*
	Cannot Login to Account with Expired Password
	Attempt to login, each time should fail
	 */
	@Test(expected = AccountExpiredException.class)
	public void noLoginWithExpiredPassword()
	{
	    willPasswordExpire(true);
		willPasswordMatch(true);
		when(account.isExpired()).thenReturn(true);
		service.login("brett", "password");
		verify(account,times(1)).setExpired(true);
	}

	/*
	Can Login to Account with Expired Password After Changing the Password
	First emulate the action to change the expired password use willPasswordExpire(false)
	Then verify this change use mockito method
	 */
	@Test
    public void loginAfterChangedExpiredPassword()
    {
        willPasswordExpire(false);
        willPasswordMatch(true);
        when(account.isExpired()).thenReturn(false);
        service.login("brett", "password");
        verify(account,times(1)).setExpired(false);
    }

    /*
    Cannot Login to Account with Temporary Password
    First, set the password as temporary password
    Then, the login function will throw an exception to fail this account to login
     */
    @Test(expected = AccountTemporaryException.class)
    public void noLoginWithTemporaryPassword()
    {
        willPasswordTemporary(true);
        willPasswordMatch(true);
        when(account.isTemporary()).thenReturn(true);
        service.login("brett", "password");
        verify(account,times(1)).setTemporary(true);
    }

    /*
    Can Login to Account with Temporary Password After Changing Password
     */
    @Test
    public void loginAfterChangedTemporaryPassword()
    {
        willPasswordTemporary(false);
        willPasswordMatch(true);
        when(account.isTemporary()).thenReturn(false);
        service.login("brett", "password");
        verify(account,times(1)).setTemporary(false);
    }

    /*
    Cannot Change Password to any of Previous 24 passwords
     */
    @Test(expected = AccountPrevious24Exception.class)
    public void noLoginWith24PreviousPassword()
    {
        willPassword24Previous(true);
        willPasswordMatch(true);
        when(account.is24Previous()).thenReturn(true);
        service.login("brett", "password");
        verify(account,times(1)).set24Previous(true);
    }

    /*
    Can Change Password to Previous password if > 24 Changes from last use
     */
    @Test
    public void loginAfterChangedPreviousPassword()
    {
        willPassword24Previous(false);
        willPasswordMatch(true);
        when(account.isTemporary()).thenReturn(false);
        service.login("brett", "password");
        verify(account,times(1)).set24Previous(false);
    }
}
