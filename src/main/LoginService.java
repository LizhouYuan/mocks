package main;

public class LoginService {
	//fix
	private final IAccountRepository accountRepository;
	private int failedAttempts = 0;
	private String previousAccountId = "";
	
	   public LoginService(IAccountRepository accountRepository) {
		   //fix
	      this.accountRepository = accountRepository;
	   }
	 

	  public void login(String accountId, String password) {
		  //fix
		      IAccount account = accountRepository.find(accountId);
		      
		      if (account == null)
		          throw new AccountNotFoundException();

		      //Password expired or not
		      if (account.passwordExpired(password))
		          account.setExpired(true);
		      else
                 account.setExpired(false);

		      //Password temporary or not
		      if (account.passwordTemporary(password))
		          account.setTemporary(true);
		      else
		          account.setTemporary(false);

                //Password is previous 24 or not
              if (account.password24Previous(password))
                  account.set24Previous(true);
              else
                  account.set24Previous(false);

		      if (account.passwordMatches(password)) {
		    	   if (account.isLoggedIn())
		               throw new AccountLoginLimitReachedException();
		    	   if (account.isRevoked())
		               throw new AccountRevokedException();
				  if (account.isExpired())
					  throw new AccountExpiredException();
				  if (account.isTemporary())
				      throw new AccountTemporaryException();
                  if (account.is24Previous())
                      throw new AccountPrevious24Exception();
		            account.setLoggedIn(true);
		       } else {
		          if (previousAccountId.equals(accountId))
		             ++failedAttempts;
		          else {
		             failedAttempts = 1;
		             previousAccountId = accountId;
		          }
		       }
		  
		       if (failedAttempts == 3)
		          account.setRevoked(true);
		    }
}
