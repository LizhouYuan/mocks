package main;

public interface IAccount {
	   void setLoggedIn(boolean value);
	   boolean passwordMatches(String candidate);
	   boolean passwordExpired(String candidate);
	   boolean passwordTemporary(String candidate);
	   boolean password24Previous(String candidate);

	   void setRevoked(boolean value);
	   void setExpired(boolean value);
	   void setTemporary(boolean value);
       void set24Previous(boolean value);

	   boolean isLoggedIn();
	   boolean isRevoked();
	   boolean isExpired();
       boolean isTemporary();
       boolean is24Previous();
}
